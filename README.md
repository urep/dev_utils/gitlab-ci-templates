# Gitlab CI Templates

<!-- badges: start -->
[![Pipeline main branch](https://forgemia.inra.fr/urep/dev_utils/gitlab-ci-templates/badges/main/pipeline.svg?key_text=Pipeline+main+branch&key_width=130)](https://forgemia.inra.fr/urep/dev_utils/gitlab-ci-templates/pipelines/main/latest)
[![Latest Release](https://forgemia.inra.fr/urep/dev_utils/gitlab-ci-templates/-/badges/release.svg)](https://forgemia.inra.fr/urep/dev_utils/gitlab-ci-templates/-/releases)
<!-- badges: end -->

This repository contains useful Gitlab CI templates files.

## Template files descriptions

| filename | description |
|---|---|
| [templates-docker.yml](/templates/templates-docker.yml) | Templates for build and push docker images |
| [templates-gitlab-docker-registry.yml](/templates/templates-gitlab-docker-registry.yml) | Templates for connection to the project Gitlab Docker Registry |
| [templates-gitlab-package-registry.yml](/templates/templates-gitlab-package-registry.yml) | Templates for download/upload to the project Gitlab Package Registry |
| [templates-checkers.yml](/templates/templates-checkers.yml) | Templates for checking some job results, for examples: checking keywords presence in logs (errors in compile log, etc.), checking uncommitted git changes, etc. |
| [templates-r.yml](/templates/templates-r.yml) | Templates around R language |

See detailed descriptions of each template job [here](/docs/Descriptions.md).

## Use the templates

1. Include the template file you want to use in you .gitlab-ci.yml (or subfile):

    ```yaml
    include:
      project:
        - project: 'urep/dev_utils/gitlab-ci-templates'
          file: '/templates/templates-<name>.yml'
    ```

2. Use template jobs in an **extends** keyword field in your .gitlab-ci.yml (or subfile) file:

    ```yaml
    jobname:
      extends:
        - .<template job name>
    ```

## Tests

This project have a CI to basically test each template.

> For the Docker tests, some push on this project registry are done. These images aren't useful after the tests. They are removed according to this project [remove policy](https://forgemia.inra.fr/urep/dev_utils/gitlab-ci-templates/-/settings/packages_and_registries) after a day.
>
> Warning: the runner docker windows tests don't pass by of issue #1 and other not fully supported config for dind in Windows inside Windows container.

## Related links

- [Gitlab yml keywords reference](https://docs.gitlab.com/ee/ci/yaml)
- [Gitlab Container Registry doc](https://docs.gitlab.com/ee/user/packages/container_registry/)
- [DRY with .gitlab-ci.yml](https://blog.stephane-robert.info/post/gitlab-template-ci/)
- [Gitlab guidelines for templates](https://docs.gitlab.com/ee/development/cicd/templates.html#template-authoring-guidelines)
