# Templates detailed descriptions

## Docker

*([templates-docker.yml](/templates/templates-docker.yml))*

- **.docker-build-and-push**  
  Builds a Docker image from a Dockerfile and push it to a docker Registry. The connection to the registry with `docker login` must be done before the `script` commands of this job.  
  Required variables:
  - `DOCKERFILE_PATH`: Path to the Dockerfile to build. Defaulted to "Dockerfile".
  - `CONTAINER_TAG`: The chosen docker tag for the new image. A default value is defined here: `$CI_REGISTRY_IMAGE:$CI_COMMIT_SHORT_SHA`.
  Optional variable:
  - `USE_CACHE`: Empty this variable to use cache.
- **.docker-push-latest**  
  Tags the Docker image for "main" branch as "latest". The connection to the registry with `docker login` must be done before the `script` commands of this job.
- **.docker-push-tag**  
  Tags the Docker image for any Git tag. The connection to the registry with `docker login` must be done before the `script` commands of this job.  
  GitLab will start a new pipeline everytime a Git tag is created, which is pretty awesome.

## Gitlab Docker Registry

*([templates-gitlab-docker-registry.yml](/templates/templates-gitlab-docker-registry.yml))*

- **.gitlab-docker-registry-login-linux**  
  Connects to the Forgemia (Gitlab) project registry for linux runners.
- **.gitlab-docker-registry-login-windows**  
  Connects to the Forgemia (Gitlab) project registry for Windows runners (powershell).

## Gitlab Package Registry

*([templates-gitlab-package-registry.yml](/templates/templates-gitlab-package-registry.yml))*

- **.gitlab-generic-package-upload**  
  Allows uploading a new package to the "package registry" of the current Gitlab project.  
  The variables `PACKAGE_NAME` and `FILENAME` must be redined. The package is stored in registry in this path: `${PACKAGE_NAME}/${GITLAB_PACKAGE_VERSION}/${FILENAME}`.  
  By default `GITLAB_PACKAGE_VERSION` is the commit SHA tag.  
  Optionaliy, variables `GITLAB_PACKAGE_VERSION` and `PATH_IN_PACKAGE_REGISTRY` cand be redefined if default are not what is expected.
- **.gitlab-generic-package-download**  
  Allows downloading an existing package from the "package registry" of the current Gitlab project.  
  Same variables rules as **.gitlab-generic-package-upload** apply to this template.
- **.gitlab-generic-package-delete**  
  > :warning: template commented, see issue #2

## Checkers

*([templates-checkers.yml](/templates/templates-checkers.yml))*

- **.check-find-bad-keywords-in-files-linux**  
  Checks in files given in variable `SEARCH_FILES` if keywords given in variable `KEYWORDS` occurs (default keywords: "error ", "warning ", "remark ").  
  If some are found then return an error but not critical for pipeline (warning).  
  The lines found are printed to console.  
  This template is for linux runners.
  > This has been primarily developped to checkers warnings in compile logs.
- **.check-find-bad-keywords-in-file-windows**  
  This is a version for Windows runners of the previous template.
- **.check-git-uncommitted-changes**  
  This job checks if the local git clone has some uncommitted changes.  
  > This is useful to ensure no versionned files have been changed by CI.
- **.check-git-notmanaged-changes**  
  This job checks if the local git clone has some unversioned files not ignored nor uncommitted changes.  
  > This is useful to ensure there are no files which must be ignored.

## R language

*([templates-r.yml](/templates/templates-r.yml))*

For the next templates, if the your package root folder is not the root directory of the git repository, then you must redefine the variable `R_PACKAGE_ROOT_RELATIVE_PATH`, which is the path to the package root folder relatively to the repository root folder. For example: `path/inside/my/repo/to/the/package/root/foler`.

- **.R-package-cache**  
  This template define the useful paths to the local packages libraries in an order which allow a cache creation. The path cached must be within the working project directory, so default ones doesn't work.  
  So this template define a cache with the key `R-installed-packages-cache`.  
  It is recommended to extend this template for all R CI jobs, because this allow some performance gain.  
  > Note: each runner has a dedicated cache, so the running job use only data in the cache of the runner is running on!
- **optionsForCi.R**  
  This is not a template, but an R function can be call before other R command in order to set some options useful for CI. These options force CI to stop on warnings and force showing additionnal call stack infos and errors messages.  
  To use this, first you need to download it in the script sectionand then the function could be called in an R command (and previous options restored at the end):

  ```yaml
  - script:
    - mkdir temp_gitlabTemplates
    - wget https://forgemia.inra.fr/urep/dev_utils/gitlab-ci-templates/-/raw/main/R/optionsForCi.R?inline=false --output-document=temp_gitlabTemplates/optionsForCi.R
    - >
      R -q -e '
        source("temp_gitlabTemplates/optionsForCi.R");

        <other R commands>

        options(optionsBackup)
        '
    - rm -rf temp_gitlabTemplates
  ```

- **.R-update-packages**  
  This template job will update all already installed R package on current runner.
- **.R-install-package-dependencies-withCache**  
  This template install all required and suggest packages of a package sources project.  
  > The default behavior of devtools functions is not to raise errors, so if an install fails this doesn't stop the CI. This template purpose is to correct that.
  > This template use the `R-installed-packages-cache` cache.
- **.R-check-documentation-is-uptodate**  
  If you forgot to update the package documentation with command `devtools::document()` before pushing you modification, then this job will failed.
- **.R-check-README-is-uptodate**  
  In case a README.Rmd is used, then a README.md must be manually generated.
  This job check if this action have been done.
- **.R-checks**  
  Run the standard R [devtools check() function](https://www.rdocumentation.org/packages/devtools/versions/1.3/topics/check).  
  By default, the license is not checked against CRAN open source supported licenses, use variable `CHECK_LICENSE` with value `"TRUE"` to activate this check.
- **.R-run-tests**  
  Run the tests for the package. This is useful to run this before the coevrage version in order to see tests results and ensure tests are ok before run the coverage below job if succeed.
- **.R-run-tests-coverage**  
  Run the tests with code coverage.  
  The code coverage results are available in three forms:
  - A percentage to be used for a badge in the README or the project page
  - An html report as artifact (file `/coverage-report.html`)
  - A Cobertura xml report for display in Gitlab merge requests diffs (with red and green lines) (files `/cobertura.xml` and `/coverage.html`)
- **.R-lintr**  
  Run [lintr](https://lintr.r-lib.org/) static code analysis.
- **.R-generate-doc-website**  
  Use [pkgdown](https://pkgdown.r-lib.org/) to generate a website for the package documentation. The generated website is in the `/docs` folder.
- **.R-build-source-package**  
  Build a source package bundle. The created file is `/build/packagename-versionnumber.tar.gz`.
- **.R-build-linux-binary-package**  
  Build a binary package bundle for linux OS. The created file is `/build/bin/packagename-versionnumber.zip`.
