# INRAe UREP Software License

## Preamble

_Version 1.0, 6 July 2022_  
_Copyright © 2022 Institut National de Recherche pour l'Agriculture, l'Alimentation et l'Environnement (INRAE)/Unité mixte de Recherche sur l'Ecosystème Prairial (UREP)_

Everyone is permitted to copy and distribute verbatim copies of this license document, but changing it is not allowed.

## TERMS AND CONDITIONS

Access and use of this software shall impose the following obligations and understandings on the user. The user is granted the right, without any fee or cost, to use, copy, modify, alter, enhance and distribute this software, and any derivative works thereof, and its supporting documentation for any purpose whatsoever, provided that this entire notice appears in all copies of the software, derivative works and supporting documentation.

Further, INRAE/UREP requests that the user credit INRAE/UREP in any publications that result from the use of this software or in any product that includes this software. The names INRAE and/or UREP, however, may not be used in any advertising or publicity to endorse or promote any products or commercial entity unless specific written permission is obtained from INRAE/UREP.

The user also understands that INRAE/UREP is not obligated to provide the user with any support, consulting, training or assistance of any kind with regard to the use, operation and performance of this software nor to provide the user with any updates, revisions, new versions or "bug fixes."

THIS SOFTWARE IS PROVIDED BY INRAE/UREP "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL INRAE/UREP BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE ACCESS, USE OR PERFORMANCE OF THIS SOFTWARE.
