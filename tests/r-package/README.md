
<!-- README.md is generated from README.Rmd. Please edit that file -->

# Gitlab templates jobs

<!-- badges: start -->

[![main
CI](https://forgemia.inra.fr/urep/dev_utils/gitlab-ci-templates/badges/main/pipeline.svg)](https://forgemia.inra.fr/urep/dev_utils/gitlab-ci-templates/-/commits/main "main branch CI pipeline status!")
[![Latest
Release](https://forgemia.inra.fr/urep/dev_utils/gitlab-ci-templates/-/badges/release.svg)](https://forgemia.inra.fr/urep/dev_utils/gitlab-ci-templates/-/releases)
[![Life cycle:
stable](https://img.shields.io/badge/lifecycle-stable-brightgreen.svg)](https://lifecycle.r-lib.org/articles/stages.html#stable)
<!-- badges: end -->

Minimal README for test
