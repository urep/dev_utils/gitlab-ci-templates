#' @title A title
#' @description A description
#' @return 4
#' @examples
#' dummy()
#' @export
dummy <- function() {
    x <- "A line way too long!---------------------------------------------------------------------------------------------------------------------------------"
    return(4)
}
